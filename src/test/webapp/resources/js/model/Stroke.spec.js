/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
describe("Stroke", function() {
    var stroke = new Stroke(new Hole(5));
    it("should give 0 as partial score when player doesn't holes the ball", function() {
        stroke.type(StrokeTypes.NONE);
        expect(stroke.score()).toEqual(0);
    });
    it("should give 5 as partial score when player holes the ball for hole 5", function() {
        stroke.type(StrokeTypes.DIRECT);
        expect(stroke.score()).toEqual(5);
    });
    it("should give 0 as partial score when player exceeds time", function() {
        stroke.type(StrokeTypes.DIRECT);
        stroke.done(false);
        expect(stroke.score()).toEqual(0);
    });
});
describe("Stroke Types", function() {
    it("should return full points when DIRECT", function() {
        expect(StrokeTypes.DIRECT.calc(100)).toEqual(100);
    });
    it("should return full points when NONE", function() {
        expect(StrokeTypes.NONE.calc(100)).toEqual(0);
    });
});
describe("Jolly Stroke", function() {
    var stroke = new JollyStroke(new Hole(5));
    it("should be a jolly stroke", function() {
        expect(isJollyStroke(stroke)).toEqual(true);
    });
    it("should give 0 as partial score when player doesn't holes the ball", function() {
        stroke.type(StrokeTypes.NONE);
        expect(stroke.score()).toEqual(0);
    });
    it("should give 10 as partial score when player holes the ball for hole 5", function() {
        stroke.type(StrokeTypes.DIRECT);
        expect(stroke.score()).toEqual(10);
    });
    it("should give 0 as partial score when player exceeds time", function() {
        stroke.type(StrokeTypes.DIRECT);
        stroke.done(false);
        expect(stroke.score()).toEqual(0);
    });
});