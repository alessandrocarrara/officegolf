/**
 * Created by acarrara on 11/28/14.
 */
describe("Hole", function() {
    it("should be created without errors", function() {
        expect(function() {new Hole(11)}).not.toThrow();
    });
    var hole = new Hole(11);
    it("should have points", function() {
        expect(hole.points()).toEqual(11);
    });
    it("should have name", function() {
        expect(hole.name()).toEqual("11");
    });
});