/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
describe("Round", function() {
    var round = new Round(25, [
        new Hole(3),
        new Hole(5),
        new Hole(7),
        new Hole(9),
        new Hole(11),
        new Hole(13),

    ], 3);
    it("should be composed of 6 strokes", function() {
        expect(round.strokes().length).toEqual(6);
    });
    it("should have a time in secs", function() {
        expect(round.time()).toEqual(25);
    });
    it("should have a well-defined jolly stroke", function() {
        expect(isJollyStroke(round.strokes()[3])).toEqual(true);
    });
    it("should throw error if jolly stroke index is out of range", function() {
        expect(function(){ new Round(25, [
                new Hole(3),
                new Hole(5),
                new Hole(7),
                new Hole(9),
                new Hole(11),
                new Hole(13),
        ], -1)}).toThrow("Jolly position out of range");
        expect(function(){ new Round(25, [
                new Hole(3),
                new Hole(5),
                new Hole(7),
                new Hole(9),
                new Hole(11),
                new Hole(13),
        ], 6)}).toThrow("Jolly position out of range");
    });
    it("should calculate 0 as total score", function() {
        expect(round.totalScore()).toEqual(0);
    });
    it("should calculate 21 as total score", function() {
        round.strokes()[0].type(StrokeTypes.DIRECT);
        round.strokes()[3].type(StrokeTypes.DIRECT);
        expect(round.totalScore()).toEqual(21);
    });
});