/**
 * Created by acarrara on 11/28/14.
 */
describe("Course", function() {
    var course = new Course();
    it("should have 6 holes", function() {
        expect(course.holes().length).toEqual(6);
    });
    it("should have 3 points for first hole", function() {
        expect(course.holes()[0].points()).toEqual(3);
    });
    it("should have 5 points for second hole", function() {
        expect(course.holes()[1].points()).toEqual(5);
    });
    it("should have 48 as max points", function() {
        expect(course.maxScore()).toEqual(48);
    });
    it("should create a round with 9 as jolly", function() {
        expect(isJollyStroke(course.newRound(25, 3).strokes()[3])).toEqual(true);
    });
});