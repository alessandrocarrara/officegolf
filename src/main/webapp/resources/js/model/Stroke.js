/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
function Stroke(hole) {

    var _type = StrokeTypes.NONE;
    var _hole = hole;
    var _done = false;

    //privileged functions
    this.type = function(newValue) {
        return isDefined(newValue) ? _setType(newValue) : _type;
    }
    this.done = function(newValue) {
        return isDefined(newValue) ? _setDone(newValue) : _done;
    }
    this.hole = function() {
        return _hole;
    }

    //private functions
    var _setType = function (newValue) {
        _type = newValue;
        _done = true;
    };
    var _setDone = function (newValue) {
        _done = newValue;
    };
}
Stroke.prototype.score = function() {
    var _this = this;
    return _this.type().calc(_this.hole().points()) * _this.done();
}

function JollyStroke(hole) {
    Stroke.apply(this, arguments);
}
JollyStroke.prototype = Object.create(Stroke.prototype);
JollyStroke.prototype.score = function() {
    return Stroke.prototype.score.call(this) * 2;
}