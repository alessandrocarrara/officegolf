/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
function Course() {

    var _holes = [
        new Hole(3),
        new Hole(5),
        new Hole(7),
        new Hole(9),
        new Hole(11),
        new Hole(13),
    ];

    //privileged functions
    this.holes = function() {
        return _holes;
    }

    this.maxScore = function() {
        var res = 0;
        for(var i = 0; i < _holes.length; i++) {
            res += _holes[i].points();
        }
        return res;
    }
}
Course.prototype.newRound = function(time, jollyPosition) {
    var _this = this;
    return new Round(time, _this.holes(), jollyPosition);
}