/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
var isDefined = function (value) {
    return (typeof value !== "undefined") && (value != null);
};
var isJollyStroke = function (value) {
    return (value instanceof JollyStroke);
};