/**
 * Created by acarrara on 11/28/14.
 */
function Hole(points) {
    var _points = points;
    var _name = String(points);

    //privileged functions
    this.points = function() {
        return _points;
    }
    this.name = function() {
        return _name;
    }

}