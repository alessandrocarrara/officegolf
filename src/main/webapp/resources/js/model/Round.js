/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
var Round = function(time, holes, jollyPosition) {

    if(jollyPosition < 0 || jollyPosition >= holes.length) {
        throw new Error("Jolly position out of range");
    }

    var _time = time;
    var _strokes = new Array();
    for(var i = 0; i < holes.length; i++) {
        _strokes.push((i === jollyPosition) ? new JollyStroke(holes[i]) : new Stroke(holes[i]));
    }

    //privileged functions
    this.time = function() {
        return _time;
    }
    this.strokes = function() {
        return _strokes;
    }
}
Round.prototype.totalScore = function() {
    var _this = this;
    var res = 0;
    var strokes = _this.strokes();
    for(var i = 0; i < strokes.length; i++) {
        res += strokes[i].score();
    }
    return res;
}