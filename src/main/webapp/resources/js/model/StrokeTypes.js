/**
 * Created by acarrara and mmiotto on 12/31/14.
 */
var StrokeType = function(multiplier) {
    var _multiplier = multiplier;

    //privileged functions
    this.calc = function(points) {
        return points * _multiplier;
    }
}

var StrokeTypes = {
    'NONE' : new StrokeType(0),
    'DIRECT' : new StrokeType(1)
    //'SECOND' : new StrokeType(1)
};